// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "animation_system.h"
#include "..\ragebot\aim.h"
#include "..\autowall\autowall.h"

void resolver::initialize(player_t* e, adjust_data* record, const float& pitch)
{

	if (g_ctx.globals.should_buy)
	{
	resolver::reset(); // reset misses, when new round start or map change
	}

	player = e;
	player_record = record;
	original_pitch = math::normalize_pitch(pitch);
}

void resolver::reset()
{
	player = nullptr;
	player_record = nullptr;

	g_ctx.globals.updating_animation = false;

	original_pitch = 0.0f;
}

void resolver::resolve_yaw()
{
	const auto state = player->get_animation_state();

	if (!state)
		return;

	resolver* data;
	adjust_data* record;
	lagcompensation* lagcomp;
	player_info_t player_info;

	if (!m_engine()->GetPlayerInfo(player->EntIndex(), &player_info))
		return;

	if (player_info.fakeplayer || !g_ctx.local()->is_alive() || player->m_iTeamNum() == g_ctx.local()->m_iTeamNum())
		return;

	// call resolver and intialize.

	lagcomp->player_resolver[player->EntIndex()].initialize(player, record, state->m_flPitch);
	lagcomp->player_resolver[player->EntIndex()].resolve_yaw();

	// some func is resolve
	bool is_player_zero = false;
	bool is_player_faking = false;
	int positives = 0;
	int negatives = 0;

	// speed is length2D..
	float speed = player->m_vecVelocity().Length2D();

	// eye delta.
	auto eye_delta = std::remainderf(math::normalize_yaw(player->m_angEyeAngles().y - player->m_flLowerBodyYawTarget()), 360.f) <= 0.f;

	// low delta detect.
	data->m_delta = fabs(player->get_max_desync_delta() < 35.f);

	// reset resolve way
	data->resolve_way = 0;

	// update animation pasted for desync.vip
	g_ctx.globals.updating_animation = false;

	// max rotation
	float max_rotation = player->get_max_desync_delta();

	// detect enemy lby.
	auto lby_detect = data->m_delta - player->m_flLowerBodyYawTarget();

	// resolve value condications.
	float resolve_value = 58.f;

	// max rotation the 33 less.
	if (max_rotation < 33.f)
		return;

	// max rotation is < resolve value use is bruteforce player accurate.
	if (max_rotation < resolve_value)
		resolve_value = max_rotation;

	// ����� � �������� �� ���������.
	if (player->IsDormant())
		return;

	// ������� ���������� - �� ���������� ����.
	if (record->shot)
		return;

	// ���� ��� �� ����� ����� �� ������ �������� �� ����� ��� ��� ����������, ��� �����.
	if (record->flags <= 1)
		return;

	// ����� �������� �� �������� � ��� �������� �� ����� - ��� ����� �� ����������?
	if (player->get_move_type() == MOVETYPE_LADDER || player->get_move_type() == MOVETYPE_NOCLIP)
		return;

	resolver_history res_history = HISTORY_UNKNOWN;

	for (auto it = lagcompensation::get().player_sets.begin(); it != lagcompensation::get().player_sets.end(); ++it)
		if (it->id == player_info.steamID64)
		{
			res_history = it->res_type;
			is_player_faking = it->faking;
			positives = it->pos;
			negatives = it->neg;

			break;
		}

	if (res_history == HISTORY_ZERO)
		is_player_zero = true;

	if (speed <= 1.1f) // enemy use always velocity

		if (!server_layers[12].m_flWeight * 1000.f && (player_record->layers[6].m_flWeight * 1000.f) == (previous_layers[6].m_flWeight * 1000.f))
		{
			float delta_first = abs(player_record->layers[6].m_flPlaybackRate - resolver_layers[0][6].m_flPlaybackRate); // none side.
			float delta_second = abs(player_record->layers[6].m_flPlaybackRate - resolver_layers[2][6].m_flPlaybackRate); // right side.
			float delta_third = abs(player_record->layers[6].m_flPlaybackRate - resolver_layers[1][6].m_flPlaybackRate); // left side.

			if (delta_third > delta_first || delta_third >= delta_second || (delta_third * 1000.0))
			{
				if (delta_first >= delta_second && delta_third > delta_third && !(delta_third * 1000.0))
				{
					data->resolve_way = 1; // right side.
					g_ctx.globals.updating_animation = true;
					data->last_anims_update_time = m_globals()->m_realtime;
				}
			}
			else
			{
				data->resolve_way = -1; // left side.
				g_ctx.globals.updating_animation = true;
				data->last_anims_update_time = m_globals()->m_realtime;
			}
			if (speed <= 0.1f)
			{
				if (player_record->layers[3].m_flWeight == 0.0f && player_record->layers[3].m_flCycle == 0.0f)
				{
					auto delta_store = std::remainderf(player_record->layers[3].m_flCycle - player_record->layers[6].m_flPlaybackRate, 360.f);
					if (2 * delta_store)
					{
						if (2 * delta_store == 2)
						{
							data->resolve_way = -1; // left
						}
					}
					else
					{
						data->resolve_way = 1; // right
					}
					g_ctx.globals.updating_animation = true;
					data->last_anims_update_time = m_globals()->m_realtime;
				}
				else
				{
					switch (g_ctx.globals.missed_shots[player->EntIndex()] % 2)
					{
					case 0:
						data->m_brute = eye_delta < 0.f ? -resolve_value : resolve_value * data->resolve_way;
						break;
					case 1:
						data->m_brute = eye_delta < 0.f ? resolve_value : -resolve_value * data->resolve_way;
						break;
					}
				}
			}
			state->m_flGoalFeetYaw = math::normalize_yaw(player->m_angEyeAngles().y + resolve_value * data->resolve_way);
		}
}

float resolver::resolve_pitch()
{
	float last_pitch;

	if (player->m_angEyeAngles().y - last_pitch > 0.5f)
		m_globals()->m_curtime;
	else
		player->m_angEyeAngles().x + last_pitch <= 0.f;

	return original_pitch;
}